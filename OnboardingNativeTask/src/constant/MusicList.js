import guitarImg from '../../assets/images/guitarImg.png';
import pianoImg from '../../assets/images/pianoImg.png';

const MusicList = [
  {
    id: 1,
    eventName: 'Guitar',
    desc: 'Guiter coaching class',
    eventImg: guitarImg,
  },
  {
    id: 2,
    eventName: 'Piano Class',
    desc: 'Piano class for kids',
    eventImg: pianoImg,
  },
  {
    id: 3,
    eventName: 'Guitar',
    desc: 'Guiter coaching class',
    eventImg: guitarImg,
  },
];
export default MusicList;
