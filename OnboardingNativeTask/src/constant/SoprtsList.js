import cricketImg from '../../assets/images/cricketImg.png';
import billiardImg from '../../assets/images/billiardsImg.png';

const SportsList = [
  {
    id: 1,
    eventName: 'Cricket',
    desc: 'Below Under -19  cricket coaching in kovaipudur',
    eventImg: cricketImg,
  },
  {
    id: 2,
    eventName: 'Billiards ',
    desc: 'Billiards and all indoor games for all ages',
    eventImg: billiardImg,
  },
  {
    id: 3,
    eventName: 'Cricket',
    desc: 'Below Under kovaipudur',
    eventImg: cricketImg,
  },
];
export default SportsList;
