import OnlineImg from '../../assets/images/OnlineImg.png';
import InPersonImg from '../../assets/images/InPersonImg.png';
import IndoorImg from '../../assets/images/IndoormImg.png';
import OutdoorImg from '../../assets/images/OutdoorImg.png';

const CategoryList = [
  {
    id: 1,
    name: 'Online',
    category_Img: OnlineImg,
  },
  {
    id: 2,
    name: 'InPerson',
    category_Img: InPersonImg,
  },
  {
    id: 3,
    name: 'Indoor',
    category_Img: IndoorImg,
  },
  {
    id: 4,
    name: 'Outdoor',
    category_Img: OutdoorImg,
  },
];

export default CategoryList;
