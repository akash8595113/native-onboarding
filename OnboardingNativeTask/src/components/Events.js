import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';

const Events = ({List}) => {
  return (
    <View>
      {/* <Text style={styles.headingText}>Exolpre by Categories</Text> */}
      <FlatList
        data={List}
        keyExtractor={list => list.id}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={element => (
          <View style={styles.listContainer}>
            <Image source={element.item.eventImg} style={styles.listImage} />
            <Text style={styles.listText}>{element.item.eventName}</Text>
            <Text style={styles.descText}>{element.item.desc}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default Events;

const styles = StyleSheet.create({
  headingText: {
    fontFamily: 'Nunito-Regular',
    color: '#000000',
    fontSize: 20,
    fontWeight: 700,
    margin: 10,
  },
  listContainer: {
    margin: 10,
  },
  listImage: {
    width: 140,
    height: 130,
  },
  listText: {
    fontFamily: 'Roboto-Bold',
    color: '#000000',
    fontSize: 15,
    fontWeight: 500,
    marginTop: 2,
  },
  descText: {
    fontFamily: 'Roboto-Bold',
    color: '#000000',
    fontSize: 8,
    fontWeight: 500,
    marginTop: 2,
    width: 130,
  },
});
