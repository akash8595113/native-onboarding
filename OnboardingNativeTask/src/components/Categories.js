import React from 'react';
import {View, Text, FlatList, Image, StyleSheet} from 'react-native';
import CategoryList from '../constant/CategoryList';

const Categories = () => {
  return (
    <View>
      <Text style={styles.headingText}>Exolpre by Categories</Text>
      <FlatList
        data={CategoryList}
        keyExtractor={list => list.id}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={element => (
          <View style={styles.listContainer}>
            <Image
              source={element.item.category_Img}
              style={styles.listImage}
            />
            <Text style={styles.listText}>{element.item.name}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default Categories;

const styles = StyleSheet.create({
  headingText: {
    fontFamily: 'Nunito-Regular',
    color: '#000000',
    fontSize: 20,
    fontWeight: 700,
    margin: 10,
  },
  listContainer: {
    margin: 10,
  },
  listImage: {
    width: 100,
  },
  listText: {
    fontFamily: 'Nunito-Regular',
    color: '#000000',
    fontSize: 15,
    fontWeight: 700,
    textAlign: 'center',
    marginTop: 2,
  },
});
