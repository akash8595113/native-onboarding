import React from 'react';
import {View, TouchableOpacity, Image, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PlayedText from '../../assets/images/PLAYEDtext.png';

const HeaderComponent = () => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <Icon name="menu" size={30} color="black" />
      </TouchableOpacity>
      <Image source={PlayedText} />
      <TouchableOpacity>
        <Icon name="send" size={30} color="black" />
      </TouchableOpacity>
    </View>
  );
};

export default HeaderComponent;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 15,
  },
});
