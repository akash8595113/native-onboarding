import React, {useState} from 'react';
import {StyleSheet, TextInput, View, Keyboard} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Searchbox = () => {
  const [searchPhrase, setSearchPhrase] = useState('');
  const [clicked, setClicked] = useState(false);

  return (
    <View style={styles.container}>
      <View
        style={
          clicked ? styles.searchBar__clicked : styles.searchBar__unclicked
        }>
        {/* search Icon */}
        <Icon name="search" size={25} color="black" style={{marginLeft: 1}} />
        {/* Input field */}
        <TextInput
          style={styles.input}
          placeholder="Type your keyword e.g., music classes..."
          value={searchPhrase}
          onChangeText={setSearchPhrase}
          onFocus={() => {
            setClicked(true);
          }}
        />
        {/* cross Icon, depending on whether the search bar is clicked or not */}
        {clicked && (
          <Icon
            name="close"
            size={25}
            color="black"
            style={{padding: 1}}
            onPress={() => {
              setSearchPhrase('');
              Keyboard.dismiss();
              setClicked(false);
            }}
          />
        )}
      </View>
    </View>
  );
};
export default Searchbox;

// styles
const styles = StyleSheet.create({
  container: {
   margin: 15,
    height: 40
  },
  searchBar__unclicked: {
    paddingLeft: 15,
    flexDirection: 'row',
 
    borderWidth: 2,
    borderColor: '#000000',
    borderRadius: 10,
    alignItems: 'center',
  },
  searchBar__clicked: {
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'row',
    width: '80%',
    borderWidth: 2,
    borderColor: '#000000',
    backgroundColor: '#d9dbda',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  input: {
    fontSize: 11,
    width: '90%',
    color: '#000',
  },
});
