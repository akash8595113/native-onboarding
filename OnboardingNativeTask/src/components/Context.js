import React, {useState} from 'react';

export const userContext = React.createContext();

const Context = ({children}) => {
    
  const [zip, setZip] = useState('');
  const [ageGroup, setAgeGroup] = useState('');

  const data = {
    zip: zip,
    ageGroup: ageGroup,
    setZip: setZip,
    setAgeGroup: setAgeGroup,
  };

  return <userContext.Provider value={data}>{children}</userContext.Provider>;
};

export default Context;
