import {Picker} from '@react-native-picker/picker';
import {useContext, useState} from 'react';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {userContext} from './Context';

const DropdownComponent = ({navigation}) => {
  const contextData = useContext(userContext);
  const {ageGroup, setAgeGroup} = contextData;
  return (  
    <View>
      <View style={styles.container}>
        <Picker
          selectedValue={ageGroup}
          style={styles.inputStyle}
          onValueChange={(itemValue, itemIndex) => setAgeGroup(itemValue)}>
          <Picker.Item label="Select Age Group" value="" />
          <Picker.Item label="0-5" value="0-5" />
          <Picker.Item label="6-10" value="6-10" />
          <Picker.Item label="10-15" value="10-15" />
          <Picker.Item label="15-18" value="15-18" />
        </Picker>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Home')}>
        <Text style={styles.buttonTextStyle}>ENTER</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    marginLeft: 110,
    width: '50%',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
  },
  buttonTextStyle: {
    color: '#ffffff',
    padding: 8,
    fontWeight: 600,
  },
  button: {
    position: 'relative',
    backgroundColor: 'rgba(255, 0, 138, 0.55)',
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 40,
    marginLeft: 130,
    borderRadius: 8,
  },
  inputStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default DropdownComponent;
