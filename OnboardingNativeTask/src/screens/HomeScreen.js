import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image, Text} from 'react-native';
import Searchbox from '../components/Searchbox';
import Categories from '../components/Categories';
import SportsList from '../constant/SoprtsList';
import MusicList from '../constant/MusicList';
import Events from '../components/Events';
import HeaderComponent from '../components/HeaderComponent';

const HomeScreen = () => {
  return (
    <View>
      {/* importing header */}
      <HeaderComponent />

      {/* importing serchbox */}
      <Searchbox />

      {/* importing category list */}
      <Categories />

      <Text style={styles.eventsText}>Nearby Top Events</Text>
      {/* Importing soprts event */}
      <View style={styles.eventContainer}>
        <Text style={styles.eventNameText}>Sports</Text>
        <TouchableOpacity>
          <Text style={styles.eventNameText}>see more</Text>
        </TouchableOpacity>
      </View>
      <Events List={SportsList} />
      <View style={styles.eventContainer}>
        <Text style={styles.eventNameText}>Music</Text>
        <TouchableOpacity>
          <Text style={styles.eventNameText}>See More</Text>
        </TouchableOpacity>
      </View>
      <Events List={MusicList} />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  eventsText: {
    margin: 10,
    color: '#000',
    fontFamily: 'Nunito-Bold',
    fontWeight: 700,
    fontSize: 20,
  },
  eventContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 12,
    marginRight: 12,
  },
  eventNameText: {
    fontFamily: 'Nunito-Bold',
    color: '#000000',
    fontSize: 15,
    fontWeight: 500,
  },
});
