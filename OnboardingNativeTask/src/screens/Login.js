import React, {useContext, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,   
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import BackgroundImg from '../../assets/images/BackgroundImg.png';
import Blob from '../../assets/images/Blob.png';
import PLAYED from '../../assets/images/PLAY-ED.png';
import DropdownComponent from '../components/DropdownComponent';
import {userContext} from '../components/Context';


const Login = ({navigation}) => {
  const contextData = useContext(userContext);
  const {zip, setZip} = contextData;
  const [getStarted, setGetStarted] = useState(false);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  return (
    <View style={styles.container}>
      <ImageBackground source={Blob} style={styles.backgroundImgStyle}>
        <Image style={styles.imgStyle} source={BackgroundImg} />
        <Image style={styles.playedTextStyle} source={PLAYED} />
        <Text style={styles.textStyle}>
          Search and list kids activities by {'\n'}your zip code
        </Text>
        {!getStarted && (
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              setGetStarted(true);
            }}>
            <Text style={styles.buttonTextStyle}>GET STARTED</Text>
          </TouchableOpacity>
        )}
        {getStarted && !isDropdownOpen && (
          <View>
            <TextInput
              placeholder="Enter ZIP code"
              keyboardType="numeric"
              style={styles.inputStyle}
              value={zip}
              onChangeText={actualValue => setZip(actualValue)}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                if (zip !== '') {
                  setIsDropdownOpen(true);
                } else {
                  Alert.alert('Please Enter Zip code');
                }
              }}>
              <Text style={styles.buttonTextStyle}>CONTINUE</Text>
            </TouchableOpacity>
          </View>
        )}
        {isDropdownOpen && <DropdownComponent navigation={navigation} />}
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  playedTextStyle: {
    marginLeft: 80,
    marginTop: 40,
    margin: 30,
  },
  imgStyle: {
    marginTop: 20,
    width: '100%',
    height: 300,
  },
  backgroundImgStyle: {
    marginTop: -40,
    width: '100%',
    height: '100%',
  },
  textStyle: {
    fontFamily: 'Nunito-Regular',
    fontWeight: 700,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
  buttonTextStyle: {
    color: '#ffffff',
    padding: 8,
    fontWeight: 600,
  },
  button: {
    position: 'relative',
    backgroundColor: 'rgba(255, 0, 138, 0.55)',
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 40,
    marginLeft: 130,
    borderRadius: 8,
  },
  inputStyle: {
    borderBottomWidth: 1,
    width: '40%',
    marginTop: 15,
    marginLeft: 110,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Login;
